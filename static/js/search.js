


function validateSearchForm(form){

    var errors = '';
    var nErrors = '';

    if ($(form.search_query).val() == '') {
        errors += nErrors + 'Query field is empty!';
        nErrors = '<br />';
    }

    if (errors != '') {
        $('#errorContentModal').html('');
        $('#errorContentModal').append(errors);
        $('#errorModal').modal('show');
        return false;
    }

    return true;
}
