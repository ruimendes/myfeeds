
function validateRegisterForm(form){

    var errors = '';
    var nErrors = '';

    if ($(form.username).val() == '') {
        errors += nErrors + 'Username field is empty!';
        nErrors = '<br />';
    }

    if ($(form.password).val() == '') {
        errors += nErrors + 'Password field is empty!';
        nErrors = '<br />';
    }

    if ($(form.password2).val() == '') {
        errors += nErrors + 'Password (repeat) field is empty!';
        nErrors = '<br />';
    }

    if ($(form.password).val() != '' && $(form.password2).val() != '') {
        if ($(form.password).val() != $(form.password2).val()) {
            errors += nErrors + 'Passwords don\'t match!';
            nErrors = '<br />';
        }
    }

    if ($(form.firstname).val() == '') {
        errors += nErrors + 'First name field is empty!';
        nErrors = '<br />';
    }

    if ($(form.lastname).val() == '') {
        errors += nErrors + 'Last name field is empty!';
        nErrors = '<br />';
    }

    if ($(form.email).val() == '') {
        errors += nErrors + 'Email field is empty!';
        nErrors = '<br />';
    }
    else {
        if (!validateEmail($(form.email).val())) {
            errors += nErrors + 'Email is invalid!';
            nErrors = '<br />';
        }
    }

    if (errors != '') {
        $('#errorContentModal').html('');
        $('#errorContentModal').append(errors);
        $('#errorModal').modal('show');
        return false;
    }

    return true;
}


function validateEmail(email) {
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
        return false;
    }
    else {
        return true;
    }
}


function clearRegistrationForm(form){

}

function showModalMessage(type, message) {

    if (type == 'success') {
        $('#successContentModal').html('');
        $('#successContentModal').append(message);
        $('#successModal').modal('show');
    }
    else if (type == 'error') {
        $('#errorContentModal').html('');
        $('#errorContentModal').append(message);
        $('#errorModal').modal('show');
    }


}

function validateLoginForm(form){

    var errors = '';
    var nErrors = '';

    if ($(form.username_login).val() == '') {
        errors += nErrors + 'Username field is empty!';
        nErrors = '<br />';
    }

    if ($(form.password_login).val() == '') {
        errors += nErrors + 'Password field is empty!';
        nErrors = '<br />';
    }

    if (errors != '') {
        $('#errorContentModal').html('');
        $('#errorContentModal').append(errors);
        $('#errorModal').modal('show');
        return false;
    }

    return true;
}

