function validateUploadRSSFile(form){

    var errors = '';
    var nErrors = '';

    if ($(form.rss).val() == '') {
        errors += nErrors + 'File field is empty!';
        nErrors = '<br />';
    }

    if (errors != '') {
        $('#errorContentModal').html('');
        $('#errorContentModal').append(errors);
        $('#errorModal').modal('show');
        return false;
    }

    return true;
}