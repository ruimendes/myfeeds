__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

from authFeeds.models import UserAccounts
from collector.scripts.solr import SolrEngine
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.encoding import smart_str

@login_required()
def index(request):
    """
    Function of search page
    """
    try:
        accs = UserAccounts.objects.get(user=request.user)
    except UserAccounts.DoesNotExist:
        accs = ''

    query_filter = 'all'

    return render(request, 'search.html', {"breadcrumb": True, 'accs': accs, 'query_filter': query_filter})


def search_feeds(query, user_id, **params):
    """
    Function to obtain results from solr of a given query
    """
    try:
        s = SolrEngine()
    except:
        raise

    search_data = s.search_data(params['params']["query_solr"])

    return search_data

@login_required()
def search_results(request):
    """
    Function to obtain results from a given query
    """
    user_id = request.user.id

    query_filter = request.POST.get('searchFilter')
    query = request.POST.get('search_query')

    if not query:
        query_solr_aux = 'user_i:' + str(user_id)

    else:
         query_solr_aux = 'user_i:' + str(user_id) + ' AND index_content_t:' + query

    if query_filter == 'all':
        query_solr = query_solr_aux
    elif query_filter == 'facebook':
        query_solr = query_solr_aux + ' AND account_t:facebook'
    elif query_filter == 'google':
        query_solr = query_solr_aux + ' AND account_t:google'
    elif query_filter == 'twitter':
        query_solr = query_solr_aux + ' AND account_t:twitter'
    elif query_filter == 'rss':
        query_solr = query_solr_aux + ' AND account_t:rss'

    try:
        accs = UserAccounts.objects.get(user=request.user)
    except UserAccounts.DoesNotExist:
        accs = ''

    try:
        result_solr = search_feeds(query, user_id, params={"query_solr": query_solr})
    except:
        result_solr = ''
        pass

    results = []
    for r in result_solr:
        if 'content_t' in r:
            results_aux = {}
            created_time = ''
            account = ''
            url = ''
            image = ''
            content = smart_str(r['content_t'])
            if 'post_created_time_t' in r:
                created_time = smart_str(r['post_created_time_t'])
            if 'account_t' in r:
                account = smart_str(r['account_t'])
            if 'url_t' in r:
                url = smart_str(r['url_t'])
            if 'image_t' in r:
                image = smart_str(r['image_t'])
            results_aux['content'] = content
            results_aux['created_time'] = created_time
            results_aux['account'] = account
            results_aux['image'] = image
            results_aux['url'] = url
            results.append(results_aux)

    return render(request, 'search.html', {"breadcrumb": True, 'results': results, 'accs': accs,
                                           'query_filter': query_filter})
