__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', 'results.views.index', name='search'),
    url(r'^results$', 'results.views.search_results', name='search_results'),

)
