__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render


@login_required()
def index(request):
    """
    Function of profiles page
    """

    return render(request, 'profiles.html', {"breadcrumb": True})
