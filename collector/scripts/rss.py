__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

from authFeeds.models import RSS
from collector.scripts.solr import SolrEngine
import random
from xml.etree import ElementTree


class RssScript():
    """
    Class to get data from RSS file, parse the data and index it to solr
    """
    def __init__(self):
        pass

    def get_feeds(self, user):
        """
        Method to collect data from RSS file
        """
        try:
            rsssacc = RSS.objects.get(user=user)
            content_file = rsssacc.file.read()
            try:
                self.delete_rss_feeds(user, account='rss')          # Delete old data
                self.save_rss_feeds(rsssacc.file, user)             # Save data of rss feeds to solr
            except:
                content_file = ''
        except RSS.DoesNotExist:
            content_file = ''

        data_result = {'data': content_file}

        return data_result


    @staticmethod
    def save_rss_feeds(url_file, user):
        """
        Method to parse and store given data
        """
        #ToDO:
        # - parse of data type

        # create a connection to a solr server
        try:
            s = SolrEngine()
        except:
            raise

        # add a document to the index

        from time import gmtime, strftime
        date_updated = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        print date_updated

        # with open("'" + data.file + "'", 'rt') as f:
        with open(str(url_file), 'rt') as f:
            tree = ElementTree.parse(f)

        results = []
        for node in tree.iter('outline'):
            indexed_content_aux = ''
            space = ' '
            content_aux = ''
            results_aux = {}
            url = ''
            image = ''
            created_time = ''
            hash_id = random.getrandbits(128)
            text = node.attrib.get('text')
            url = node.attrib.get('htmlUrl')
            if text and url:
                content_aux = text
                indexed_content_aux = text
                url = url

            results_aux = {"id": hash_id, "user_i": user.id, "account_t": "rss",
                                   "date_updated_t": str(date_updated), 'index_content_t': indexed_content_aux,
                                   'content_t': content_aux, 'url_t': url, 'post_created_time_t': created_time,
                                   'image_t': image}
            results.append(results_aux)

        print results

        try:
            s.index_data(results)
            rss = RSS.objects.get(user=user.id)
            rss.latest_date.now()
            rss.save()
        except:
            raise

    @staticmethod
    def delete_rss_feeds(user, account='rss'):
        """
        Method to remove facebook data from solr
        """
        try:
            s = SolrEngine()
            query = 'user_i:' + str(user.id) + ' AND account_t:' + str(account)
            s.delete(q=query)
        except:
            raise