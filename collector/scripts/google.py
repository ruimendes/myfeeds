__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

import random
import httplib2
from authFeeds.models import Google
from collector.scripts.solr import SolrEngine
import json
from django.utils.encoding import smart_str


class GoogleScript():
    """
    Class to get data from google, parse the data and index it to solr
    """
    def __init__(self):
        self._url_activities = "https://www.googleapis.com/plus/v1/people/me/activities/public/?access_token="
        self._url_playlists = "https://www.googleapis.com/youtube/v3/playlists?part=snippet&mine=true&access_token="
        self._url_playlist_detail = "https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&access_token="

    def get_feeds(self, access_token, user, feed='activities'):
        """
        Method to collect data from google
        """
        if feed == 'activities':
            url = self._url_activities + access_token
        elif feed == 'playlists':
            url = self._url_playlists + access_token

        h = httplib2.Http()
        content = h.request(url, "GET")


        data = ''

        if content[0].status == 200:

            data = json.loads(content[1])
            try:
                self.delete_google_feeds(user, account='google')    # Delete old data
                self.save_google_feeds(data, user)          # Save data of google feeds to solr
            except:
                data = ''
        else:
            data = ''

        data_result = {'data': data}
        return data_result

    def get_playist_detail(self, access_token, playlist_id):
        """
        Method to get playlist detail
        """

        url = self._url_playlist_detail + access_token + "&playlistId=" + playlist_id
        h = httplib2.Http()
        content = h.request(url, "GET")
        return content

    @staticmethod
    def save_google_feeds(data, user):
        """
        Method to parse and store given data
        """
        #ToDO:
        # - parse of data type

        # create a connection to a solr server
        try:
            s = SolrEngine()
        except:
            raise

        from time import gmtime, strftime

        date_updated = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        # print date_updated

        import re

        regex = re.compile(r'[\n\r\t"]')
        results = []

        # print data['items']
        for d in data['items']:
            indexed_content_aux = ''
            space = ' '
            content_aux = ''
            results_aux = {}
            url = ''
            image = ''
            created_time = ''
            hash_id = random.getrandbits(128)

            try:
                if 'object' in d:
                    if 'content' in d['object']:
                        str_aux = d['object']['content'].replace("\"", "").replace("\n", " ").replace("\t", " ")
                        indexed_content_aux += space + str_aux
                        content_aux = str_aux
                    if 'attachments' in d['object']:
                        for attach in d['object']['attachments']:

                            if 'content' in attach:
                                str_aux = attach['content'].replace("\"", "").replace("\n", " ").replace("\t", " ")
                                indexed_content_aux += space + str_aux
                            if 'displayName' in attach:
                                str_aux = attach['displayName'].replace("\"", "").replace("\n", " ").replace("\t", " ")
                                indexed_content_aux += space + str_aux
                            if attach['objectType'] == 'photo':
                                if 'image' in attach:
                                    if 'url' in attach['image']:
                                        image = attach['image']['url']
                if 'published' in d:
                    created_time = d['published']
                if 'url' in d:
                    url = d['url']

                results_aux = {"id": hash_id, "user_i": user.id, "account_t": "google",
                               "date_updated_t": str(date_updated), 'index_content_t': indexed_content_aux,
                               'content_t': content_aux, 'url_t': url, 'post_created_time_t': created_time,
                               'image_t': image}
                results.append(results_aux)

            except:
                pass
        print results

        try:
            s.index_data(results)
            google = Google.objects.get(user=user.id)
            google.latest_date.now()
            google.save()
        except:
            raise


    @staticmethod
    def delete_google_feeds(user, account='google'):
        """
        Method to google data from solr
        """
        try:
            s = SolrEngine()
            query = 'user_i:' + str(user.id) + ' AND account_t:' + str(account)
            print query
            s.delete(q=query)
        except:
            raise