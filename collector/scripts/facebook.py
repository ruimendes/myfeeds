__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

import json
import random
import httplib2
from authFeeds.models import Facebook
from collector.scripts.solr import SolrEngine
from django.utils.encoding import smart_str


class FacebookScript():
    """
    Class to get data from facebook, parse the data and index it to solr
    """
    def __init__(self):
        self._url = "https://graph.facebook.com/me/feed?access_token="

    def get_feeds(self, access_token, user):
        """
        Method to collect data from facebook
        """
        h = httplib2.Http()
        url = self._url + access_token
        content = h.request(url, "GET")

        if content[0]['status'] == '200':
            data = json.loads(smart_str(content[1]))
            try:
                self.delete_facebook_feeds(user, account='facebook')    # Delete old data
                self.save_facebook_feeds(data, user)                    # Save data of facebook feeds to solr
            except:
                data = ''
        else:
            data = ''

        return data

    @staticmethod
    def save_facebook_feeds(data, user):
        """
        Method to parse and store given data
        """

        # create a connection to a solr server
        try:
            s = SolrEngine()
        except:
            raise

        # add a document to the index
        from time import gmtime, strftime

        date_updated = strftime("%Y-%m-%d %H:%M:%S", gmtime())

        import re
        regex = re.compile(r'[\n\r\t"]')

        results = []
        for d in data['data']:
            indexed_content_aux = ''
            space = ' '
            content_aux = ''
            results_aux = {}
            url = ''
            image = ''
            created_time = ''
            hash_id = random.getrandbits(128)

            try:

                if d['type'] == 'status':
                    if 'story' in d:
                        str_aux = d['story'].replace("\"", "").replace("\n", " ").replace("\t", " ")
                        indexed_content_aux += space + str_aux
                        content_aux = str_aux
                    if 'created_time' in d:
                        created_time = d['created_time']
                    if 'link' in d:
                        url = d['link']
                    if 'from' in d:
                        if 'name' in d['from']:
                            str_aux = d['from']['name'].replace("\"", "").replace("\n", " ").replace("\t", " ")
                            indexed_content_aux += space + str_aux
                    results_aux = {"id": hash_id, "user_i": user.id, "account_t": "facebook",
                                   "date_updated_t": str(date_updated), 'index_content_t': indexed_content_aux,
                                   'content_t': content_aux, 'url_t': url, 'post_created_time_t': created_time,
                                   'image_t': image}
                    results.append(results_aux)

                elif d['type'] == 'link':

                    if 'story' in d:
                        str_aux = d['story'].replace("\"", "").replace("\n", " ").replace("\t", " ")
                        indexed_content_aux += space + str_aux
                        content_aux = str_aux
                    if 'created_time' in d:
                        created_time = d['created_time']
                    if 'link' in d:
                        url = d['link']
                    if 'picture' in d:
                        image = d['picture']
                    results_aux = {"id": hash_id, "user_i": user.id, "account_t": "facebook",
                                   "date_updated_t": str(date_updated), 'index_content_t': indexed_content_aux,
                                   'content_t': content_aux, 'url_t': url, 'post_created_time_t': created_time,
                                   'image_t': image}
                    results.append(results_aux)

                elif d['type'] == 'photo':

                    if 'message' in d:
                        str_aux = d['message'].replace("\"", "").replace("\n", " ").replace("\t", " ")
                        indexed_content_aux += space + str_aux
                        content_aux = str_aux
                    if 'created_time' in d:
                        created_time = d['created_time']
                    if 'link' in d:
                        url = d['link']
                    if 'picture' in d:
                        image = d['picture']
                    results_aux = {"id": hash_id, "user_i": user.id, "account_t": "facebook",
                                   "date_updated_t": str(date_updated), 'index_content_t': indexed_content_aux,
                                   'content_t': content_aux, 'url_t': url, 'post_created_time_t': created_time,
                                   'image_t': image}
                    results.append(results_aux)

            except:
                pass

        try:
            s.index_data(results)
            facebook = Facebook.objects.get(user=user.id)
            facebook.latest_date.now()
            facebook.save()
        except:
            raise

    @staticmethod
    def delete_facebook_feeds(user, account='facebook'):
        """
        Method to remove facebook data from solr
        """
        try:
            s = SolrEngine()
            query = 'user_i:' + str(user.id) + ' AND account_t:' + str(account)
            print query
            s.delete(q=query)
        except:
            raise
