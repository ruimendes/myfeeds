__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

from MyFeeds import settings
import pysolr


class SolrEngine:
    """It is responsible for index the documents and search over them
    It also connects to SOLR
    """
    
    CONNECTION_TIMEOUT_DEFAULT = 10
    # Make this unique, and don't share it with anybody.
    SECRET_KEY = 'j*zdirg7yy9@q1k=c*q!*kovfsd#$FDFfsdfkae#id04pyssta=yz@w34m6rvwfe'

    def __init__(self, timeout=CONNECTION_TIMEOUT_DEFAULT):
        # Setup a Solr instance. The timeout is optional.
        self.solr = pysolr.Solr('http://' + settings.SOLR_HOST + ':' + settings.SOLR_PORT + '/solr', timeout=timeout)

    def index_data(self, doc):
        """Index fingerprint 
        """
        # index document
        print "INDEX_DATA: ",
        print doc
        self.index_data_as_json(doc)
    
    def index_data_as_json(self, d):
        """Index fingerprint as json
        """
        # index document
        
        try:
            print "XML_ANSWER_1: ",
            print d
            xml_answer = self.solr.add(d)
            print(xml_answer)
        except:
            raise
        self.optimize()

    def optimize(self):
        """This function optimize the index. It improvement the search and retrieve documents subprocess.
        However, it is a hard tasks, and call it for every index document might not be a good idea.
        """
        self.solr.optimize()

    def update(self, doc):
        """Update the document
        """
        # Search  and identify the document id

        self.solr.add([doc])

    def delete(self, id_doc=None, q=None):
        """Delete the document
        """
        self.solr.delete(id=id_doc, q=q)

    def search_data(self, query, start=0, rows=10000, fl=''):
        """search data from user
        """
        # Later, searching is easy. In the simple case, just a plain Lucene-style
        # query is fine.
        print query
        results = self.solr.search(query, **{
                'rows': rows,
                'start': start,
                'fl': fl
            })

        return results

    # def more_like_this(self, id_doc):
    #     similar = self.solr.more_like_this(q='id:doc_2', mltfl='text')
    #     return similar
