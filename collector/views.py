__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

from collector.scripts.facebook import FacebookScript
from collector.scripts.google import GoogleScript
from collector.scripts.rss import RssScript
from django.shortcuts import render, redirect
from authFeeds.models import Facebook, Google, UserAccounts, Twitter, RSS
from django.contrib.auth.decorators import login_required

@login_required()
def index(request):
    """
    Function of collector page
    """

    debug = False

    try:
        accs = UserAccounts.objects.get(user=request.user)
    except UserAccounts.DoesNotExist:
        accs = ''

    try:
        facebok_last_date = Facebook.objects.get(user=request.user.id).latest_date
    except Facebook.DoesNotExist:
        facebok_last_date = ''

    try:
        google_last_date = Google.objects.get(user=request.user.id).latest_date
    except Google.DoesNotExist:
        google_last_date = ''

    try:
        twitter_last_date = Twitter.objects.get(user=request.user.id).latest_date
    except Twitter.DoesNotExist:
        twitter_last_date = ''

    try:
        rss_last_date = RSS.objects.get(user=request.user.id).latest_date
    except RSS.DoesNotExist:
        rss_last_date = ''

    last_date = {'facebook': facebok_last_date, 'google': google_last_date, 'twitter': twitter_last_date,
                 'rss': rss_last_date}

    return render(request, 'collect.html', {"breadcrumb": True, "debug": debug,
                                            'last_date': last_date, 'accs': accs})


@login_required()
def collect_data(request, account_id):
    """
    Function to coleect data from an account or RSS file
    """
    debug = False
    content = ''
    print account_id

    try:
        accs = UserAccounts.objects.get(user=request.user)
    except UserAccounts.DoesNotExist:
        accs = ''

    try:
        #Facebook
        if account_id == '1':
            account = FacebookScript()
            fbuser = Facebook.objects.get(user=request.user)
            content = account.get_feeds(fbuser.access_token, request.user)

        #Google
        elif account_id == '2':
            account = GoogleScript()
            gouser = Google.objects.get(user=request.user)
            content = account.get_feeds(gouser.access_token, request.user, 'activities')

        #Twitter
        elif account_id == '3':
            pass

        #RSS
        elif account_id == '4':
            account = RssScript()
            content = account.get_feeds(request.user)

        if content:
            message = 'success'
        else:
            message = 'error'


    except:
        message = 'error'

    try:
        facebok_last_date = Facebook.objects.get(user=request.user.id).latest_date
    except Facebook.DoesNotExist:
        facebok_last_date = ''

    try:
        google_last_date = Google.objects.get(user=request.user.id).latest_date
    except Google.DoesNotExist:
        google_last_date = ''

    try:
        twitter_last_date = Twitter.objects.get(user=request.user.id).latest_date
    except Twitter.DoesNotExist:
        twitter_last_date = ''

    try:
        rss_last_date = RSS.objects.get(user=request.user.id).latest_date
    except RSS.DoesNotExist:
        rss_last_date = ''

    last_date = {'facebook': facebok_last_date, 'google': google_last_date, 'twitter': twitter_last_date,
                 'rss': rss_last_date}

    return render(request, 'collect.html', {"content": content, "message": message, "debug": debug, 'accs': accs,
                                            'last_date': last_date})
