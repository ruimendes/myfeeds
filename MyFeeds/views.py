__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

from authFeeds.models import UserAccounts
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import render


def index(request):
    """
    Function of initial page (login)
    """
    #if request.session['statusFeed']:
    #    status = request.session['statusFeed']
    #else:
    #    status = ''

    status = request.GET.get('status')

    return render(request, 'index.html', {'status': status})


@login_required()
def home(request):
    """
    Function of dashboard page
    """

    try:
        accs = UserAccounts.objects.get(user=request.user)
    except UserAccounts.DoesNotExist:
        accs = ''

    print accs

    return render(request, 'home.html', {'breadcrumb': True, 'accs': accs})

@login_required()
def profile(request):
    """
    Function of user profile page
    """
    return render(request, 'profile.html', {'breadcrumb': True})


def project(request):
    """
    Function of project page
    """
    return render(request, 'project.html', {'breadcrumb': True})


def team(request):
    """
    Function of team page
    """
    return render(request, 'team.html', {'breadcrumb': True})