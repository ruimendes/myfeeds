__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', 'MyFeeds.views.index', name='index'),
    url(r'^myfeeds/$', 'MyFeeds.views.home', name='home'),
    url(r'^profile/$', 'MyFeeds.views.profile', name='profile'),
    url(r'^project/$', 'MyFeeds.views.project', name='project'),
    url(r'^team/$', 'MyFeeds.views.team', name='team'),
    # url(r'^contacts/$', 'MyFeeds.views.contacts', name='contacts'),

    url(r'^accounts/', include('authFeeds.urls')),
    url(r'^collect/', include('collector.urls')),
    url(r'^profiles/', include('profiles.urls')),
    url(r'^search/', include('results.urls')),


    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),

)
