__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

import os
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from authFeeds.storage import OverwriteStorage


class UserAccounts(models.Model):
    """
    User Accounts Model
    """

    user = models.ForeignKey(User, unique=True)
    facebook = models.BooleanField('Facebook account', default=False)
    twitter = models.BooleanField('Twitter account', default=False)
    google = models.BooleanField('Google account', default=False)
    rss = models.BooleanField('RSS file', default=False)

    def __unicode__(self):
        return u'%s' % self.user

    @property
    def count_accounts(self):
        """
        Method to get the number of associated accounts
        """
        n = 0
        if self.facebook:
            n += 1
        if self.twitter:
            n += 1
        if self.google:
            n += 1
        if self.rss:
            n += 1
        return n


class Facebook(models.Model):
    """
    Facebook Model
    """
    user = models.ForeignKey(User, unique=True)
    username = models.CharField(max_length=100, unique=True)
    access_token = models.TextField(blank=False)
    refresh_token = models.TextField(blank=True)
    create_date = models.DateTimeField(auto_now_add=True)
    latest_date = models.DateTimeField(auto_now=True)


class Twitter(models.Model):
    """
    Twitter Model
    """
    user = models.ForeignKey(User, unique=True)
    username = models.CharField(max_length=100, unique=True)
    access_token = models.TextField(blank=False)
    secret_token = models.TextField(blank=True)
    create_date = models.DateTimeField(auto_now_add=True)
    latest_date = models.DateTimeField(auto_now=True)


class Google(models.Model):
    """
    Google Model
    """
    user = models.ForeignKey(User, unique=True)
    username = models.CharField(max_length=100, unique=True)
    access_token = models.TextField(blank=False)
    refresh_token = models.TextField(blank=True)
    create_date = models.DateTimeField(auto_now_add=True)
    latest_date = models.DateTimeField(auto_now=True)


def update_filename(instance, filename):
    if not str(filename).endswith(".opml"):
        raise ValidationError("Extension not allowed!")
    filedir = "rssfiles/"+str(instance.user.id)+".opml"
    return os.path.join(filedir)


class RSS(models.Model):
    """
    RSS Model
    """
    user = models.ForeignKey(User, unique=True)
    file = models.FileField(upload_to=update_filename, storage=OverwriteStorage())
    create_date = models.DateTimeField(auto_now_add=True)
    latest_date = models.DateTimeField(auto_now=True)

