__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^$', 'authFeeds.views.index', name='index'),
    url(r'^facebook$', 'authFeeds.views.fbcback', name='fbcback'),
    url(r'^google$', 'authFeeds.views.gocback', name='gocback'),
    url(r'^register$', 'authFeeds.views.register', name='register'),
    url(r'^associate$', 'authFeeds.views.associate', name='associate'),
    url(r'^rssupload$', 'authFeeds.views.rssupload', name='rssupload'),
    url(r'^testfb$', 'authFeeds.views.testfacebook', name='testfacebook'),
    url(r'^testgo$', 'authFeeds.views.testgoogle', name='testgoogle'),
    url(r'^remove$', 'authFeeds.views.remove_account', name='remove'),


    url(r'^login$', 'authFeeds.views.login_view', name='login'),
    url(r'^logout$', 'authFeeds.views.logout_view', name='logout'),



)
