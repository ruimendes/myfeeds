__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

import json
import urllib
from collector.scripts.facebook import FacebookScript
from collector.scripts.google import GoogleScript
from collector.scripts.rss import RssScript
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from authFeeds.forms import RegistrationForm, LoginForm
from django.contrib.auth.models import User
from django.shortcuts import render, redirect
import httplib2
from authFeeds.models import Facebook, Google, UserAccounts, RSS
import datetime


@login_required()
def index(request):
    """
    Function of accounts page
    """
    try:
        accs = UserAccounts.objects.get(user=request.user)
    except UserAccounts.DoesNotExist:
        accs = ''

    latest_date = ''

    if accs.rss:
        rssacc = RSS.objects.get(user=request.user)
        latest_date = rssacc.latest_date

    return render(request, 'accounts.html', {'breadcrumb': True, 'accs': accs, 'error': False, 'rssdate': latest_date})


def fbcback(request):
    """
    Function to get credentials data from facebook
    """

    # FACEBOOK VARS
    app_id = "388207784642528"
    redirect_url = "http://localhost:8000/accounts/facebook"
    app_secret = "4e97911c6cc12322ba84a334dcd49b64"

    # GET CODE AND ERROR VALUE FROM URL
    code = request.GET.get('code')
    error = request.GET.get('error')

    # PERFORM DATA FETCH
    if code:
        ## GET ACCESS TOKEN
        url = "https://graph.facebook.com/oauth/access_token?client_id="+app_id+"&redirect_uri="+\
              redirect_url+"&client_secret="+app_secret+"&code="+code;
        h1 = httplib2.Http()
        content = h1.request(url, "GET")
        access_token = content[1].replace("access_token=", "")

        request.session["ftoken"] = access_token

        ##GET USER DATA
        graphurl = "https://graph.facebook.com/me?access_token="+access_token
        h2 = httplib2.Http()
        response = h2.request(graphurl, "GET")

        ##GET USER JSON FROM RESPONSE
        user_data = json.loads(response[1])

        ## PARSE JSON TO GET IMPORTANT DATA
        firstname = user_data['first_name']
        lastname = user_data['last_name']
        username = user_data['username']
        email = user_data['email']

        ## CHECK FOR FACEBOOK ACCOUNT
        if Facebook.objects.filter(username=username).count():
            if request.user.is_authenticated():
                return redirect("/accounts")
            fb = Facebook.objects.get(username=username)
            logout(request)
            fb.user.backend = "django.contrib.auth.backends.ModelBackend"
            login(request, fb.user)
            return redirect("/myfeeds")
        else:
            request.session["user"] = {'type': "facebook", 'firstname': firstname, 'lastname': lastname,
                                       'username': username, 'email': email, 'access_token': access_token}
            return redirect("/accounts/associate")

    ##HANDLE ERRORS
    if error:
        return render(request, 'error.html', {'error': error})


## GOOGLE STUFF
def gocback(request):
    """
    Function to get credentials data from google
    """

    #GOOGLE VARS
    client_id = "230193286070.apps.googleusercontent.com"
    client_secret = "VubyUQeX8voxWN_FWKlaY-87"
    redirect_url = "http://localhost:8000/accounts/google"

    # GET CODE AND ERROR VALUE FROM URL
    code = request.GET.get('code')
    error = request.GET.get('error')

    # PERFORM DATA FETCH
    if code:

        ## GET ACCESS TOKEN
        url = "https://accounts.google.com/o/oauth2/token"
        headers = {'Content-type': 'application/x-www-form-urlencoded'}
        body = {'code': code, 'client_id': client_id, 'client_secret': client_secret, 'redirect_uri': redirect_url, 'grant_type':'authorization_code'}

        h1 = httplib2.Http()
        response, content = h1.request(url, 'POST', headers=headers, body=urllib.urlencode(body))
        access_token = json.loads(content)['access_token']

        ##GET USER DATA
        url2 = "https://www.googleapis.com/oauth2/v1/userinfo?access_token="+access_token
        h2 = httplib2.Http()
        response = h2.request(url2, "GET")

        user_data = json.loads(response[1])

        firstname = user_data['given_name']
        lastname = user_data['family_name']
        username = user_data['email']
        email = user_data['email']

        ## CHECK FOR GOOGLE ACCOUNT

        if Google.objects.filter(username=username).count():
            if request.user.is_authenticated():
                return redirect("/accounts")
            go = Google.objects.get(username=username)
            logout(request)
            go.user.backend = "django.contrib.auth.backends.ModelBackend"
            login(request, go.user)
            return redirect("/myfeeds")
        else:
            request.session["user"] = {'type': "google", 'firstname': firstname, 'lastname': lastname, 'username': username, 'email': email, 'access_token': access_token}
            return redirect("/accounts/associate")

    ##HANDLE ERRORS
    if error:
        return render(request, 'autherror.html', {'error': error})


def register(request, template_name='register.html'):
    """
    Function to register a new user
    """
    if request.method == 'POST':
        form = RegistrationForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']

            try:

                user = User()
                user.username = username
                user.first_name = first_name
                user.last_name = last_name
                user.email = email
                user.set_password(password)
                user.save()

                uac = UserAccounts()
                uac.user = user
                uac.save()

                return redirect('/?status=success')

            except:
                status = 'error'
                # raise

            #SEND EMAIL TO ADMINS
            #subject = "Contacto MyFeeds - "
            #message = "Contacto do site nkp.pt, com os seguintes dados:\n\nNome: " + str(name) + "\nEmail: " +
            # str(email) + "\nAssunto: " + str(subject) + "\nMensagem: " + str(message)
            #if cc_myself:
            #    send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, [email, ])
            #
            ## Send email to admins
            #email_list = []
            #for k, v in settings.ADMINS:
            #    email_list.append(v)
            #send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, email_list)

            return render(request, template_name, {'form': form, 'request': request, 'status': status})

    else:
        form = RegistrationForm()

    return render(request, template_name, {'form': form, 'request': request})


def login_view(request):
    """
    Function to call login form
    """
    form = LoginForm(request.POST or None)
    if request.POST and form.is_valid():
        user = form.login(request)
        if user.is_active:
            logout(request)
            login(request, user)
            return redirect('/myfeeds')
    #        # Redirect to a success page.
    return render(request, 'index.html', {'form': form, 'request': request})


def logout_view(request):
    """
    Function to logout
    """
    logout(request)
    return redirect('/')


def associate(request):
    """
    Function to associate account to the user
    """
    if request.method == "POST":

        error = False
        error_message = ''

        act = request.POST.get("act")
        user = request.session.get("user")

        acc_type = user["type"]
        username = user["username"]
        firstname = user["firstname"]
        lastname = user["lastname"]
        email = user["email"]
        access_token = user["access_token"]

        #Register
        if act == "register":

            ## MAIL MUST BE UNIQUE
            if User.objects.filter(email=email).exists():
                return render(request, 'accmanager.html', {'request': request, 'error': True,
                                                           'error_message': 'Email already registered!'})

            new_user = User()
            new_user.username = username
            new_user.first_name = firstname
            new_user.last_name = lastname
            new_user.email = email
            new_user.set_password("123")
            new_user.save()

            uac = UserAccounts()
            uac.user = new_user

            if acc_type == "facebook":
                fb = Facebook()
                fb.user = new_user
                fb.username = username
                fb.access_token = access_token
                fb.save()

                uac.facebook = True
                uac.save()

            if acc_type == "google":

                go = Google()
                go.user = new_user
                go.username = username
                go.access_token = access_token
                go.save()

                uac.google = True
                uac.save()

            new_user.backend = "django.contrib.auth.backends.ModelBackend"
            login(request, new_user)

            return redirect("/myfeeds")

        #Associate
        if act == "associate":
            form = LoginForm(request.POST or None)

            if request.POST:
                if not form.is_valid():
                    error = True
                    error_message = "Username/Password don't match!"

                else:

                    userf = form.login(request)

                    if userf.is_active:
                        print "COISOOOOOOOOO"
                        uac = UserAccounts.objects.get(user=userf)

                        if acc_type == "facebook":
                            print "COISOOOOOOOOO22222222222"
                            try:
                                fb = Facebook()
                                fb.user = userf
                                fb.username = username
                                fb.access_token = access_token
                                fb.save()
                            except:
                                error = True
                                error_message = "Account already associated!"

                            uac.facebook = True
                            uac.save()

                        if acc_type == "google":
                            print "COISOOOOOOOOO33333333333333"
                            try:
                                go = Google()
                                go.user = userf
                                go.username = username
                                go.access_token = access_token
                                go.save()
                            except:
                                error = True
                                error_message = "Account already associated!"

                            if not error:
                                uac.google = True
                                uac.save()
                            else:
                                return render(request, 'accmanager.html', {'form': form, 'request': request, 'error': error,
                                                       'error_message': error_message})

                        logout(request)
                        login(request, userf)
                        return redirect('/accounts')
            #        # Redirect to a success page.
            return render(request, 'accmanager.html', {'form': form, 'request': request, 'error': error,
                                                       'error_message': error_message})

    if request.method == "GET":
        return render(request, 'accmanager.html')


def rssupload(request):
    """
    Function do uplaod RSS file
    """
    error = False
    error_message = ""

    uacs = UserAccounts.objects.get(user=request.user)

    if not uacs.rss:
        rssacc = RSS()
        rssacc.user = request.user
        rssacc.file = request.FILES['rss']

        try:
            rssacc.save()
            uacs.user = request.user
            uacs.rss = True
            uacs.save()
        except:
            error = True
            error_message = "Invalid file extension!"

    else:
        rssacc = RSS.objects.get(user=request.user)
        rssacc.latest_date = datetime.datetime.now()
        rssacc.file = request.FILES['rss']

        try:
            rssacc.save()
        except:
            error = True
            error_message = "Invalid file extension!"

    return render(request, "accounts.html", {'request': request, 'accs': uacs, 'error': error,
                                             'error_message': error_message, 'rssdate': rssacc.latest_date,
                                             'breadcrumb': True})


def testfacebook(request):
    """
    Function to test facebook credentials
    """

    user = Facebook.objects.get(user=request.user)
    access_token = user.access_token

    graphurl = "https://graph.facebook.com/me?access_token="+access_token
    h2 = httplib2.Http()
    response = h2.request(graphurl, "GET")

    status = response[0]["status"]

    if status == "200":
        return HttpResponse("<span class='label label-success'><i class='fa fa-check'></i> Good</span>")

    else:
        return HttpResponse("<span class='label label-danger'><i class='fa fa-exclamation-circle'></i> Error</span>")
    

def testgoogle(request):
    """
    Function to test google credentials
    """
    user = Google.objects.get(user=request.user)
    access_token = user.access_token

    url = "https://www.googleapis.com/oauth2/v1/userinfo?access_token="+access_token
    h2 = httplib2.Http()
    response = h2.request(url, "GET")

    status = response[0]["status"]

    if status == "200":
        return HttpResponse("<span class='label label-success'><i class='fa fa-check'></i> Good</span>")

    else:
        return HttpResponse("<span class='label label-danger'><i class='fa fa-exclamation-circle'></i> Error</span>")


def remove_account(request):
    """
    Function to remove one account of user's account
    """
    account = request.POST.get("account")

    print account
    if account == 'facebook':
        try:
            user = Facebook.objects.get(user=request.user).delete()
            userAcc = UserAccounts.objects.get(user=request.user)
            userAcc.facebook = False
            userAcc.save()
            fb = FacebookScript()
            fb.delete_facebook_feeds(request.user, account='facebook')
            status = 'success'
        except Facebook.DoesNotExist, UserAccounts.DoesNotExist:
            status = 'error'

    elif account == 'google':
        try:
            user = Google.objects.get(user=request.user).delete()
            userAcc = UserAccounts.objects.get(user=request.user)
            userAcc.google = False
            userAcc.save()
            goo = GoogleScript()
            goo.delete_google_feeds(request.user, account='google')
            status = 'success'
        except Google.DoesNotExist, UserAccounts.DoesNotExist:
            status = 'error'

    elif account == 'rss':
        try:
            user = RSS.objects.get(user=request.user).delete()
            userRss = UserAccounts.objects.get(user=request.user)
            userRss.rss = False
            userRss.save()
            rss = RssScript()
            rss.delete_rss_feeds(request.user, account='rss')
            status = 'success'
        except RSS.DoesNotExist, UserAccounts.DoesNotExist:
            status = 'error'

    if status == "success":
        return HttpResponse("<span class='label label-success'><i class='fa fa-check'></i> Account removed with success!</span>")

    else:
        return HttpResponse("<span class='label label-danger'><i class='fa fa-exclamation-circle'></i> Error! the account wasn't removed!</span>")