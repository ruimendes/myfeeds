__author__ = 'Joao Martins & Rui Mendes'
"""
Projecto: MyFeedsEngine
Disciplina: Arquiteturas Distribuidas (44157)
Professor: Claudio Teixeira
Data de entrega: 13/12/13
Autores: Joao Martins (41904) & Rui Mendes (67021)
"""

from django.contrib.auth import authenticate
from django import forms
from django.contrib.auth.models import User


class RegistrationForm(forms.ModelForm):
    """
    Class of the form to register a new account
    """
    password2 = forms.CharField(max_length=128)
    first_name = forms.CharField(max_length=30, )
    last_name = forms.CharField(max_length=30)
    email = forms.EmailField()

    class Meta:
        model = User
        fields = ('username', 'password')

    def clean_username(self):
        """
        Method to check if username already exists
        """
        cleaned_data = super(RegistrationForm, self).clean()
        username = cleaned_data.get("username")
        try:
            User.objects.get(username=username)
            raise forms.ValidationError("That username already exists.")
        except User.DoesNotExist:
            return username

    def clean_email(self):
        """
        Method to check if email already exists
        """
        cleaned_data = super(RegistrationForm, self).clean()
        email = cleaned_data.get("email")
        username = cleaned_data.get("username")
        if email and User.objects.filter(email=email).exclude(username=username).count():
            raise forms.ValidationError("Email address already exists.")
        return email

    def clean(self):
        """
        Method to check if the two passwords match
        """
        cleaned_data = super(RegistrationForm, self).clean()
        if cleaned_data.get("password") != cleaned_data.get("password2"):
              raise forms.ValidationError("The passwords do not match.")

        return self.cleaned_data


class LoginForm(forms.Form):
    """
    Class of the form to login
    """
    username_login = forms.CharField(max_length=255, required=True)
    password_login = forms.CharField(widget=forms.PasswordInput, required=True)

    def clean(self):
        """
        Method to validate user credentials
        """
        username = self.cleaned_data.get('username_login')
        password = self.cleaned_data.get('password_login')
        user = authenticate(username=username, password=password)
        if not user or not user.is_active:
            raise forms.ValidationError("Sorry, that login was invalid. Please try again.")
        return self.cleaned_data

    def login(self, request):
        """
        Method to authenticate the user
        """
        username = self.cleaned_data.get('username_login')
        password = self.cleaned_data.get('password_login')
        user = authenticate(username=username, password=password)
        return user