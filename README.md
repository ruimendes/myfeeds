My Feeds Engine

This project was made for academic purposes, for the course 'Distributed Architectures' of the Master's in Information Systems @ University of Aveiro

MyFeeds Engine is a web platform that allows the user to see content of all his social networks in one place. The information is indexed and the user can search for any keyword to find posts or other relevant content. Besides social network information the user can read his feeds too.